using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DEI_MVC_APP.Models;
using DEI_WS_LOCAL.Services;

namespace DEI_MVC_APP.Controllers
{
    public class TestRemoteController : Controller
    {
        private readonly ITestingServices _testingServices;   

        public TestRemoteController (ITestingServices testingServices){
            
            _testingServices = testingServices;

        }       
        public async Task<IActionResult> Test ()
        {
            //var result = await _testingServices.getResponseWS1();
            return View("result");
        }
    }
}