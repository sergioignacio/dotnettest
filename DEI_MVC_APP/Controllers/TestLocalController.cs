using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DEI_MVC_APP.Models;
using DEI_WS_LOCAL.Services;

namespace DEI_MVC_APP.Controllers
{
    public class TestLocalController : Controller
    {
        public readonly ITestingServices _testingServices;   

        public TestLocalController(ITestingServices testingServices){
            
            _testingServices = testingServices;

        }       
       public IActionResult Test ()
        {
            ViewBag.Message = _testingServices.getResponseWSLocal();
            
            return View();
        }
    }
}